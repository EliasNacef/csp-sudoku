#include <point.h>

/*
 * Constructor: Point
 * Usage: Point origin;
 *        Point pt(x, y);
 * ----------------------
 * Creates a Point object with the specified x and y coordinates.  If the
 * coordinates are not supplied, the default constructor sets these fields
 * to 0.
 */

Point::Point(): m_x(0), m_y(0)
{

}
Point::Point(int x, int y) : m_x(x), m_y(y) {}

/*
 * Method: getX
 * Usage: int x = pt.getX();
 * -------------------------
 * Returns the x-coordinate of the point.
 */

int Point::GetX() const
{
    return m_x;
}

/*
 * Method: getY
 * Usage: int y = pt.getY();
 * -------------------------
 * Returns the y-coordinate of the point.
 */

int Point::GetY() const
{
    return m_y;
}

/*
* Method: setX
* Usage: pt.setX(int x);
* -------------------------
* Set the x-coordinate of the point.
*/

void Point::SetX(int x)
{
    m_x = x;
}

/*
* Method: setY
* Usage: pt.setY(int y);
* -------------------------
* Set the y-coordinate of the point.
*/

void Point::SetY(int y)
{
    m_y = y;
}