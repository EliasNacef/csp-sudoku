
#include <common.h>
#include <sudoku/sudoku.h>
#include <csp.h>
#include <backtracking.h>
int main(int argc, char* argv[])
{
	// Program must run with two parameters
	if (argc != 2)
	{
		std::cout << "The program works with one parameter : the path for the sudoku" << std::endl;
		std::cout << "Exemple : ./csp-sudoku.exe file.ss" << std::endl;
		return EXIT_FAILURE;
	}
	// Start chronometer
	auto start = std::chrono::steady_clock::now();

	// Sudoku creation
	Sudoku sudoku(argv[1]);
	sudoku.Disp();

	// Create CSP and run the AI
	Csp csp(sudoku.GetSudoku());
	bool result = Backtracking::BacktrackingSearch(csp);
	std::cout << "end.." << std::endl;

	// End Chrono
	auto end = std::chrono::steady_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

	// Disp result
	csp.GetAssignement().Disp();
	if (!result) std::cout << "failure.. duration : " << duration.count() << "ms" << std::endl;
	else std::cout << "success! duration : " << duration.count() << "ms" << std::endl;

    std::cin.ignore();
    return EXIT_SUCCESS;
}