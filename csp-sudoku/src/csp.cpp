
#include <csp.h>

Csp::Csp(vector<vector<int>> sudoku) : m_assignement(sudoku)
{
	// Assignement initialization 
	for (size_t row = 0; row < sudoku.size(); row++)
	{
		for (size_t column = 0; column < sudoku[row].size(); column++)
		{
			int sudokuValue = sudoku[row][column];
			Point location(row, column);
			if (sudokuValue == 0)
			{
				// Sudoku hole (empty space)
				vector<int> domain = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }; // initial domain with every value possible
				auto var = make_shared<Variable>(location, domain, 0);
				m_assignement.AddVariable(var);
			}
			else
			{
				// Already a value in this space
				vector<int> domain = { sudokuValue };
				auto var = make_shared<Variable>(location, domain, sudokuValue);
				m_assignement.AddVariable(var);
			}
		}
	}

	// Update Variable linking (hidden graph structure)
	// And Update each variable domain
	UpdateAssignement();
}

/// <summary>
/// Update domains and edges
/// </summary>
void Csp::UpdateAssignement()
{
	auto& variables = m_assignement.Variables();
	for (auto& current : variables)
	{
		current->ClearEdges();
		current->ResetDomain();
		Point pCurrent = current->GetLocation();
		for (auto& other : variables)
		{
			Point pOther = other->GetLocation();
			if (IsRelative(*current, *other))
			{
				// other could constrains current
				int otherValue = other->GetValue();
				if (otherValue > 0)
				{
					current->AddEdgeTo(other);
					current->RemoveFromDomain(otherValue); // Remove other from domain if required
				}
			}
		}
	}
	AC3();
}


void Csp::AC3()
{
	queue<pair<Variable&, Variable&>> queue;
	const auto& variables = m_assignement.Variables();
	for (auto& variable : variables)
	{
		const auto& linkedVariables = variable->GetEdgedVariables();
		for (auto& linked : linkedVariables)
		{
			std::pair<Variable&, Variable&> p(*variable, *linked);
			queue.push(p);
		}
	}

	while (!queue.empty())
	{
		const std::pair<Variable&, Variable&> first = queue.front();
		auto& xi = first.first;
		auto& xj = first.second;
		queue.pop();
		if (RemoveInconsistentValues(xi, xj))
		{
			const auto& xiNeighbors = xi.GetEdgedVariables();
			for (auto& xk : xiNeighbors)
			{
				const std::pair<Variable&, Variable&> pki(*xk, xi);
				queue.push(pki);
			}
		}
	}
}

bool Csp::RemoveInconsistentValues(Variable& xi, Variable& xj)
{
	bool removed = false;
	const Point& xiPoint = xi.GetLocation();
	const Point& xjPoint = xj.GetLocation();
	const auto& xiDomain = xi.GetDomain();
	const auto& xjDomain = xj.GetDomain();
	for (auto& x : xiDomain)
	{
		bool consistent = false;
		for (auto& y : xjDomain)
		{
			if (!IsConstrained(xiPoint, x, xjPoint, y))
			{
				consistent = true;
				break;
			}
		}
		if (!consistent)
		{
			xi.RemoveFromDomain(x);
			removed = true;
		}
	}
	return removed;
}

bool Csp::IsValueConsistent(const Variable& toCheck, const int wishedValue) const
{
	const auto& wishedPosition = toCheck.GetLocation();
	const auto& variables = m_assignement.GetVariables();
	for (auto& variable : variables)
	{
		const auto& varPos = variable->GetLocation();
		const auto& varValue = variable->GetValue();

		if (IsConstrained(wishedPosition, wishedValue, varPos, varValue)) return false;
	}
	return true;
}


bool Csp::IsConstrained(const Variable& a, const Variable& b) const
{
	const Point aPoint = a.GetLocation();
	const Point bPoint = b.GetLocation();
	const int aValue = a.GetValue();
	const int bValue = b.GetValue();
	return IsConstrained(aPoint, aValue, bPoint, bValue);;
}

bool Csp::IsConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const
{
	if (aPoint == bPoint || aValue != bValue) return false;
	if (IsRowConstrained(aPoint, aValue, bPoint, bValue)) return true;
	if (IsColumnConstrained(aPoint, aValue, bPoint, bValue)) return true;
	if (IsSquareConstrained(aPoint, aValue, bPoint, bValue)) return true;
	return false;
}

bool Csp::IsRelative(const Variable& a, const Variable& b) const
{
	const Point& aPoint = a.GetLocation();
	const Point& bPoint = b.GetLocation();
	if (aPoint == bPoint) return false;
	if (IsRowRelative(aPoint, bPoint) || IsColumnRelative(aPoint, bPoint))
	{
		return true; // same row or column
	}

	return IsSquareRelative(aPoint, bPoint);
}


bool Csp::IsRowRelative(const Point& aPoint, const Point& bPoint) const
{
	return aPoint.GetX() == bPoint.GetX();
}

bool Csp::IsColumnRelative(const Point& aPoint, const Point& bPoint) const
{
	return aPoint.GetY() == bPoint.GetY();
}

bool Csp::IsSquareRelative(const Point& aPoint, const Point& bPoint) const
{
	const int bX = bPoint.GetX();
	const int bY = bPoint.GetY();
	const int aX = aPoint.GetX();
	const int aY = aPoint.GetY();
	// Will check in current square thanks to aPoint offset (square is 3 sized)
	const int aSquareLineOffset = aX - aX % 3;
	const int aSquareColumnOffset = aY - aY % 3;
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t j = 0; j < 3; j++)
		{
			const int xSquare = aSquareLineOffset + i;
			const int ySquare = aSquareColumnOffset + j;
			if (xSquare == bX && ySquare == bY)
			{
				return true; // Same square belonging
			}
		}
	}
	return false;
}

bool Csp::IsRowConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const
{
	return(aValue == bValue && IsRowRelative(aPoint, bPoint));
}


bool Csp::IsColumnConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const
{
	return(aValue == bValue && IsColumnRelative(aPoint, bPoint));
}

bool Csp::IsSquareConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const
{
	// Check value
	if (aValue != bValue) return false;
	// Check square belonging
	return IsSquareRelative(aPoint, bPoint);
}


bool Csp::IsAssignementConsistent() const
{
	const auto& variables = m_assignement.GetVariables();
	for (auto& variable : variables)
	{
		Point location = variable->GetLocation();
		int value = variable->GetValue();
		int consistency = variable->Consistency();
		if (consistency != 0)
		{
			return false;
		}
	}
	return true;
}

bool Csp::IsAssignementComplete() const
{
	const auto& variables = m_assignement.GetVariables();
	for(auto& variable : variables)
	{
		if (variable->GetValue() == 0)
		{
			return false;
		}
	}
	return true;
}


shared_ptr<Variable> Csp::SelectUnassignedVariable()
{
	auto& variables = m_assignement.Variables();

	// Apply Variable heuristic with sort
	std::sort(variables.begin(), variables.end(),
		[&](const shared_ptr<Variable>& a, const shared_ptr<Variable>& b) -> bool
	{
		return VariableHeuristic(*a, *b);
	});

	for(auto& variable : variables)
	{
		if (variable->GetValue() <= 0)
		{
			// Apply Value Heuristic for choosen variable
			auto& domain = variable->Domain();
			std::sort(domain.begin(), domain.end(),
				[&](const int& a, const int& b) -> bool
			{
				return ValueHeuristic(*variable, a, b);
			});
			return variable;
		}
	}
	return nullptr;
}

bool Csp::VariableHeuristic(const Variable& a, const Variable& b) const
{
	// MRV
	const size_t aDomainSize = a.GetDomain().size();
	size_t bDomainSize = b.GetDomain().size();
	if (aDomainSize < bDomainSize) return true;
	else if (aDomainSize > bDomainSize) return false;

	const size_t aConsistency = a.Consistency();
	const size_t bConsistency = b.Consistency();
	// Degree Heuristic
	if (aConsistency > bConsistency) return true;
	else if (aConsistency < bConsistency) return false;
	return false;
}

bool Csp::ValueHeuristic(const Variable& var, const int& aVal, const int& bVal) const
{
	// Least Constraining value
	int aConsistency = EvaluateConsistency(var, aVal);
	int bConsistency = EvaluateConsistency(var, bVal);
	if (aConsistency < bConsistency) return true;
	else if (aConsistency > bConsistency) return false;
	return false;
}

int Csp::EvaluateConsistency(const Variable& toCheck, const int wishedValue) const
{
	int estimatedConsistency = 0;
	const auto& edgedVariables = toCheck.GetEdgedVariables();
	for (auto& variable : edgedVariables)
	{
		const auto& domain = variable->GetDomain();
		if (std::find(domain.begin(), domain.end(), wishedValue) != domain.end()) estimatedConsistency++;
	}
	return estimatedConsistency;
}