#include "assignement.h"

Assignement::Assignement(vector<vector<int>> sudoku)
{

}


void Assignement::AddVariable(shared_ptr<Variable> var)
{
	m_variables.push_back(var);
}

const vector<shared_ptr<Variable>>& Assignement::GetVariables() const
{
	return m_variables;
}

vector<shared_ptr<Variable>>& Assignement::Variables()
{
	return m_variables;
}



shared_ptr<Variable> Assignement::GetVariable(const Point& location) const
{
	for (auto& var : m_variables)
	{
		if (var->GetLocation() == location) return var;
	}
	return nullptr;
}

void Assignement::Disp() const
{
	std::vector<std::vector<int>> sudokuGrid;
	vector<int> sudokuLine((size_t) std::sqrt(m_variables.size()));
	int value = 0;
	fill(sudokuLine.begin(), sudokuLine.end(), value);
	for (size_t i = 0; i < sudokuLine.size(); i++)
	{
		sudokuGrid.push_back(sudokuLine);
	}

	for (size_t i = 0; i < m_variables.size(); i++)
	{
		Point p = m_variables[i]->GetLocation();
		sudokuGrid[p.GetX()][p.GetY()] = m_variables[i]->GetValue();
	}

	std::string dashLine = "";
	std::string spaceLine = "";
	for (size_t j = 0; j < sudokuGrid.size(); j++)
	{
		dashLine += "---";
		spaceLine += "   ";
	}
	std::cout << dashLine << std::endl;

	for (size_t j = 0; j < sudokuGrid.size(); j++)
	{
		std::cout << " ";
		for (size_t i = 0; i < sudokuGrid[j].size(); i++)
		{
			std::cout << "|";
			if (i > 0 && i % 3 == 0)
			{
				std::cout << "  ";
				std::cout << "|";
			}
			if (sudokuGrid[i][j] <= 0) std::cout << ".";
			else std::cout << sudokuGrid[i][j];
		}
		std::cout << "|" << std::endl;
		if (j < sudokuGrid.size() - 1 && (j + 1) % 3 == 0)
		{
			std::cout << spaceLine << std::endl;
			std::cout << spaceLine << std::endl;
		}
	}
	std::cout << dashLine << std::endl;
}