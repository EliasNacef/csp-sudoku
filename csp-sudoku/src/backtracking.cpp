#include <backtracking.h>

bool Backtracking::BacktrackingSearch(Csp& csp)
{
	return RecursiveBacktracking(csp);
}

bool Backtracking::RecursiveBacktracking(Csp& csp)
{
	std::cout << "search..." << std::endl;
	csp.GetAssignement().Disp();

	if (csp.IsAssignementComplete())
	{
		return true; // csp has now a complete and consistent assignement
	}
	else
	{
		auto variable = csp.SelectUnassignedVariable();
		if (variable == nullptr)
		{
			return false;
		}
		const vector<int> domain = variable->Domain();
		for(auto& dValue : domain)
		{
			if (csp.IsValueConsistent(*variable, dValue))
			{
				variable->AssignValue(dValue);
				csp.UpdateAssignement();
				bool result = RecursiveBacktracking(csp);
				if (result)
				{
					return result;
				}
				else
				{
					variable->Reset();
					csp.UpdateAssignement();
				}
			}
		}
		return false;
	}

}