#include <sudoku/sudoku.h>
#include <ctime>
#include <common.h>


/// <summary>
/// ctor with particular .ss file
/// </summary>
/// <param name="filePath">the path of the .ss file</param>
Sudoku::Sudoku(string filePath)
{
    ifstream file;

    string fileLine;
    int numberOfLine = CountLine(filePath);

    file.open(filePath);

    for (int x = 0; x < numberOfLine; x++) {
        file >> fileLine;
        std::vector<int> column;
        if (fileLine != "---!---!---")
        {
            for (int j = 0; j < 11; j++)
            {
                if (fileLine[j] != '!')
                {
                    if (fileLine[j] == '.')
                    {
                        column.push_back(0);
                    }
                    else
                    {
                        column.push_back(fileLine[j] - '0');
                    }
                }
            }
            m_grid.push_back(column);
        }
    }
    m_size = m_grid.size();
    file.close();
}



void Sudoku::Disp()
{
	std::string dashLine = "";
	std::string spaceLine = "";
    for (size_t j = 0; j < m_size; j++)
    {
        dashLine += "---";
        spaceLine += "   ";
    }
	std::cout << dashLine << std::endl;

	for (size_t j = 0; j < m_size; j++)
	{
        std::cout << " ";
		for (size_t i = 0; i < m_size; i++)
		{
			std::cout << "|";
            if (i > 0 && i % 3 == 0)
            {
                std::cout << "  ";
                std::cout << "|";
            }
            if (m_grid[i][j] <= 0) std::cout << ".";
			else std::cout << m_grid[i][j];
		}
		std::cout << "|" << std::endl;
        if (j < m_size - 1 && (j+1) % 3 == 0)
        {
            std::cout << spaceLine << std::endl;
            std::cout << spaceLine << std::endl;
        }
	}
	std::cout << dashLine << std::endl;
}

int Sudoku::GetSize()
{
	return m_size;
}

int Sudoku::CountLine(string filePath)
{
    int numberOfLines = 0;
    std::string line;
    std::ifstream file(filePath);

    while (std::getline(file, line))
    {
        ++numberOfLines;
    }
    file.close();
    return numberOfLines;
}

std::vector<std::vector<int>> Sudoku::GetSudoku()
{
    return m_grid;
}