
#include <variable.h>


const vector<shared_ptr<Variable>>& Variable::GetEdgedVariables() const
{
	return m_edgedVariables;
}

int Variable::Consistency() const
{
	return m_edgedVariables.size();
}

vector<int>& Variable::Domain()
{
	return m_domain;
}


void Variable::AddEdgeTo(shared_ptr<Variable> linkedVar)
{
	m_edgedVariables.push_back(linkedVar);
}


void Variable::RemoveEdgeTo(shared_ptr<Variable> linkedVar)
{
	vector<shared_ptr<Variable>>::iterator it;
	it = std::find(m_edgedVariables.begin(), m_edgedVariables.end(), linkedVar);
	m_edgedVariables.erase(it);
}

void Variable::ClearEdges()
{
	m_edgedVariables.clear();
}


void Variable::RemoveFromDomain(int v)
{
	m_domain.erase(std::remove(m_domain.begin(), m_domain.end(), v), m_domain.end());
}

void Variable::ResetDomain()
{
	if (m_value > 0)
	{
		m_domain = { m_value };
	}
	else
	{
		m_domain = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	}
}


void Variable::AssignValue(int value)
{
	if (value > 0)
	{
		m_value = value;
	}
	else
	{
		throw "Trying to assign a negative value for the variable !";
	}
}


void Variable::Reset()
{
	// The only manner to reset a variable because setter can't
	m_value = 0;
}