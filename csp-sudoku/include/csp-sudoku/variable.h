#pragma once
#include <common.h>
#include <point.h>

/// <summary>
/// Define a variable for a CSP assignement with int domain
/// </summary>
class Variable
{
public: 
	Variable(Point emplacement, vector<int> domain, int value): m_location(emplacement), m_domain(domain), m_value(value)
	{

	}

	Variable(const Variable&) = default;
	Variable(Variable&&) = default;


	int GetValue() const
	{
		return m_value;
	}

	const Point& GetLocation() const
	{
		return m_location;
	}

	const vector<int>& GetDomain() const
	{
		return m_domain;
	}

	const vector<shared_ptr<Variable>>& GetEdgedVariables() const;

	/// <summary>
	/// Return consistency : number of edges
	/// </summary>
	/// <returns> consistency </returns>
	int Consistency() const;

	vector<int>& Domain();


	/// <summary>
	/// Add edge from this to variable parameter
	/// </summary>
	/// <param name="linkedVar"> pointer to varaible to link </param>
	void AddEdgeTo(shared_ptr<Variable> linkedVar);

	/// <summary>
	/// Remove the edge from this to variable parameter if it exists
	/// </summary>
	/// <param name="linkedVar"></param>
	void RemoveEdgeTo(shared_ptr<Variable> linkedVar);
	/// <summary>
	/// Clear edge vector
	/// </summary>
	void ClearEdges();

	/// <summary>
	/// Remove a value from domain
	/// </summary>
	/// <param name="v"> value to remove </param>
	void RemoveFromDomain(int v);

	/// <summary>
	/// Reset domain with sudoku values if required
	/// </summary>
	void ResetDomain();



	/// <summary>
	/// Assign positive value for variable
	/// </summary>
	/// <param name="value"> A strictly positive value </param>
	void AssignValue(int value);

	/// <summary>
	/// Reset the variable
	/// </summary>
	void Reset();

private:
	Point m_location; // Location in sudoku
	vector<int> m_domain; // Possibles values for variable
	int m_value; // Value in sudoku
	vector<shared_ptr<Variable>> m_edgedVariables; // Variables with which it is possibly constrained (lines, columns and squares but not zeros)
};

