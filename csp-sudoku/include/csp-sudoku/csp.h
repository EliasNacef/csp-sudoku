#pragma once
#include <common.h>
#include <variable.h>
#include <assignement.h>
#include <queue>

class Csp
{
public:
	Csp(vector<vector<int>> sudoku);
	Csp(const Csp&) = delete;

	void UpdateAssignement();

	/// <summary>
	/// Arc consistency 3
	/// </summary>
	void AC3();
	bool RemoveInconsistentValues(Variable& xi, Variable& xj);

	/// <summary>
	/// Is a variable consistent with a wanted value ?
	/// </summary>
	/// <param name="toCheck"> variable to check </param>
	/// <param name="wishedValue"> value to check in variable toCheck </param>
	/// <returns> True / False according to answer to question </returns>
	bool IsValueConsistent(const Variable& toCheck, int wishedValue) const;

	/// <summary>
	/// Return the number of linked variable with wished value in their domain
	/// </summary>
	/// <param name="toCheck"> variable to check </param>
	/// <param name="wishedValue"> value to test </param>
	/// <returns> Number of variable concerned </returns>
	int EvaluateConsistency(const Variable& toCheck, int wishedValue) const;

	/// <summary>
	/// Is two variables constrained ?
	/// </summary>
	/// <param name="a"> first variable </param>
	/// <param name="b"> second variable </param>
	/// <returns> true if they are constrained </returns>
	bool IsConstrained(const Variable& a, const Variable& b) const;
	bool IsConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const;

	/// <summary>
	/// Is a variable relative to another ? (careful about value of the oter)
	/// </summary>
	/// <param name="a"> first variable </param>
	/// <param name="b"> second variable </param>
	/// <returns> true if they are relatives </returns>
	bool IsRelative(const Variable& a, const Variable& b) const;

	/// <summary>
	/// Is current assignement consistent ?
	/// </summary>
	/// <returns> true/false </returns>
	bool IsAssignementConsistent() const;

	/// <summary>
	/// Is current assignement complete ?
	/// </summary>
	/// <returns> true/false </returns>
	bool IsAssignementComplete() const;

	const Assignement& GetAssignement()
	{
		return m_assignement;
	}

	/// <summary>
	/// Return an unassigned variable to assign in order to complete the assignement
	/// </summary>
	/// <returns> A pointer towards an unassigned variable (nullptr if all variables are assigned) </returns>
	shared_ptr<Variable> SelectUnassignedVariable();

private:
	Assignement m_assignement;

	/// <summary>
	/// MRV and Degree heuristics
	/// </summary>
	/// <param name="a"> first variable </param>
	/// <param name="b"> second variable </param>
	/// <returns></returns>
	bool VariableHeuristic(const Variable& a, const Variable& b) const;

	/// <summary>
	/// Least Constrained Value Heuristic
	/// </summary>
	/// <param name="var"> variable concerned </param>
	/// <param name="a"> first variable </param>
	/// <param name="b"> second variable </param>
	/// <returns></returns>
	bool ValueHeuristic(const Variable& var, const int& a, const int& b) const;


	/// <summary>
	/// Are the two points row relatives in sudoku ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="bPoint"> second point </param>
	/// <returns> are they row relatives ? (true/false) </returns>
	bool IsRowRelative(const Point& aPoint, const Point& bPoint) const;

	/// <summary>
	/// Are the two points column relatives in sudoku ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="bPoint"> second point </param>
	/// <returns> are they column relatives ? (true/false) </returns>
	bool IsColumnRelative(const Point& aPoint, const Point& bPoint) const;

	/// <summary>
	/// Are the two points square relatives in sudoku ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="bPoint"> second point </param>
	/// <returns> are they square relatives ? (true/false) </returns>
	bool IsSquareRelative(const Point& aPoint, const Point& bPoint) const;

	/// <summary>
	/// Are the two (point, value) pairs row constrained in the csp ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="aValue"> first value </param>
	/// <param name="bPoint"> second point </param>
	/// <param name="bValue"> second value</param>
	/// <returns> true or false </returns>
	bool IsRowConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const;

	/// <summary>
	/// Are the two (point, value) pairs column constrained in the csp ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="aValue"> first value </param>
	/// <param name="bPoint"> second point </param>
	/// <param name="bValue"> second value</param>
	/// <returns> true or false </returns>
	bool IsColumnConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const;


	/// <summary>
	/// Are the two (point, value) pairs square constrained in the csp ?
	/// </summary>
	/// <param name="aPoint"> first point </param>
	/// <param name="aValue"> first value </param>
	/// <param name="bPoint"> second point </param>
	/// <param name="bValue"> second value</param>
	/// <returns> true or false </returns>
	bool IsSquareConstrained(const Point& aPoint, const int& aValue, const Point& bPoint, const int& bValue) const;
};
