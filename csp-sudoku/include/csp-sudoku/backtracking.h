#pragma once
#include <csp.h>

/// <summary>
/// Class handling backtracking seach
/// </summary>
class Backtracking
{
public:
	bool static BacktrackingSearch(Csp& csp);

private:
	bool static RecursiveBacktracking(Csp& csp);
};