#pragma once
#include <iostream>
#include<string>

using namespace std;

class Point
{
public:

    /*
     * Constructor: Point
     * Usage: Point origin;
     *        Point pt(x, y);
     * ----------------------
     * Creates a Point object with the specified x and y coordinates.  If the
     * coordinates are not supplied, the default constructor sets these fields
     * to 0.
     */

    Point();
    Point(int x, int y);

    /*
     * Method: getX
     * Usage: int x = pt.getX();
     * -------------------------
     * Returns the x-coordinate of the point.
     */

    int GetX() const;

    /*
     * Method: getY
     * Usage: int y = pt.getY();
     * -------------------------
     * Returns the y-coordinate of the point.
     */

    int GetY() const;

    /*
     * Method: setX
     * Usage: pt.setX(int x);
     * -------------------------
     * Set the x-coordinate of the point.
     */

    void SetX(int x);

    /*
     * Method: setY
     * Usage: pt.setY(int y);
     * -------------------------
     * Set the y-coordinate of the point.
     */
    void SetY(int y);

    /*Return grid distance */
    static int ManhattanDistance(Point a, Point b)
    {
        return abs(a.GetX() - b.GetX()) + abs(a.GetY() - b.GetY());
    }

    bool operator<(const Point& p2) const
    {
        return m_x < p2.GetX() && m_y < p2.GetY();
    }
    bool operator>(Point p2)
    {
        return m_x > p2.GetX() && m_y > p2.GetY();
    }
    bool operator==(Point p2)
    {
        return m_x == p2.GetX() && m_y == p2.GetY();
    }
    bool operator==(const Point p2) const
    {
        return m_x == p2.GetX() && m_y == p2.GetY();
    }
    bool operator!=(Point p2)
    {
        return m_x != p2.GetX();
    }


private: 
    int m_x;
    int m_y;

};

