#pragma once
#include <common.h>
#include <variable.h>


class Assignement
{
public:

	Assignement(vector<vector<int>> sudoku);
	Assignement(const Assignement&) = delete;

	/// <summary>
	/// Add a variable to variables collection
	/// </summary>
	/// <param name="var"> Variable to add </param>
	void AddVariable(const shared_ptr<Variable> var);

	const vector<shared_ptr<Variable>>& GetVariables() const;
	vector<shared_ptr<Variable>>& Variables();
	shared_ptr<Variable> GetVariable(const Point& location) const;

	/// <summary>
	/// Disp variables in console
	/// </summary>
	void Disp() const;

private:
	vector<shared_ptr<Variable>> m_variables; 
};

