#pragma once
#include <vector>
#include<string>
#include <fstream>
/// <summary>
/// Sudoku class
/// </summary>
class Sudoku
{
public:
	/// <summary>
	/// ctor with particular .ss file
	/// </summary>
	/// <param name="filePath">the path of the .ss file</param>
	Sudoku(std::string filePath);

	Sudoku(const Sudoku& sudoku) = default;

	~Sudoku() = default;

	/// <summary>
	/// Disp sudoku in console
	/// </summary>
	void Disp();

	int GetSize();

	/// <summary>
	/// Return the sudoku grid
	/// </summary>
	/// <returns></returns>
	std::vector<std::vector<int>> GetSudoku();

private:
	std::vector<std::vector<int>> m_grid;
	int m_size;

	int CountLine(std::string filePath);
};

